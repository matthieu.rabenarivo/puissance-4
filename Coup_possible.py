def coup_possible(gril,col):
    '''
    Fonction coup_possible(gril, col) :
    Détermine si possible de jouer dans la colone col
    Prend en argument la grille, tableau de 5 x 6, avec la position des pions des joueurs et un entier
    le numéro de colone entre 0 et 6.
    Renvoie True si possible de,jouer dans la colonne col, False sinon.
    iL EST POSSIBLE DE JOUER DANS LA COLONE COL si il existe une case avec la valeur 0 dans cette colonne
    
    
    '''
    for i in gril:
        if i[col]==0:
            return True
    return False

assert coup_possible([[0,1,1],[1,1,1],[1,0,1]], 2)==False
assert coup_possible([[1],[1],[1]],0)==False
assert coup_possible([[0,0,0],[0,0,0],[0,0,0]], 1)==True , 1
