def horiz(gril,j,lig,col):
    '''
    Fonction horiz(gril, j, lig, col):
      Détermine si il y a un alignement vertical de 4 pions du joueurs j
      à partir de la case (lig,col).
    arguments:
      gril la grille avec les pions.
      j le joueur, un entier avec la valeur 1 ou 2
      lig la ligne, un entier avec la valeur entre 0 et 5
      col la colone , un entier avec la valeur entre 0 et 3
    Renvoie True si c est le cas.
    '''
    nbr=0
    for i in range(4):
        if gril[lig][col+i]==j:
            nbr=nbr+1
    if nbr==4:
        return True
    return False
assert horiz([[0,0,0,0],[1,1,1,0],[1,1,1,1]],2,0,0)==False
assert horiz([[0,0,0,0],[1,1,1,0],[1,1,1,1]],1,2,0)==True
assert horiz([[0,0,0,0],[1,1,1,0],[1,1,1,1]],1,1,0)==False
assert horiz([[0,0,0,0],[1,1,1,0],[1,1,1,1]],2,1,0)==False
assert horiz([[0,0,0,0],[1,1,1,0],[1,1,1,1]],0,0,0)==True



