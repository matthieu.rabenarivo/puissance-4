def grille_vide():
    '''
Fonction grille vide():
La fonction à deux dimensions de taille 6 x 7 :
6 lignes et 7 colones.
Chaque case contient la valeur 0.
La fonction ne prend pas d'argument.
La fonction renvoie le tableau
'''
    tableau = []
    for i in range (6):
        tableau.append([0,0,0,0,0,0,0])
    return tableau
assert len(grille_vide())==6
assert grille_vide()[0][0]==0
assert grille_vide()[5][6]==0
assert len(grille_vide()[3])==7
grille_vide()
