def vert(gril,j,lig, col):
    '''
    Fonction vert(gril,lig,col):
    Détermine si il y a un alignement vertical de 4 pions
    du joueur j à partir de la case (lig , col).
    Arguments:
      gril la grille avec les pions.
      j le joueur, un entier avec la valeur 1 ou 2
      lig la ligne, un entier avec la valeur entre 0 et 2
      col la colonne, un entier avec la valeur entre 0 et 6
    Renvoie True si c est le cas, False sinon.
    '''
    nbr=0
    for i in range(4):
        if gril[lig+i][col]==j:
            nbr=nbr+1
    if nbr==4:
        return True
    return False
    
