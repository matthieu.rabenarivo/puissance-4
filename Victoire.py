def victoire(gril,j):
    for lig in range(len(gril)):
        for col in range(len(gril[0])):
            if col<=3:
                if horiz(gril,j,lig,col)==True:
                    return True
            if lig<=2:
                if vert(gril,j,lig,col)==True or diag_haut(gril,j,lig,col)==True or diag_bas(gril,j,lig,col)==True:
                    return True                
                
            
    return False 
assert victoire([[0,0,0,0,0,0,0],[1,1,1,1,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]],1)==True
assert victoire([[0,0,0,0,0,0,0],[1,1,1,1,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]],2)==False
assert victoire([[0,0,0,2,0,0,0],[1,1,2,1,0,0,0],[0,2,0,0,0,0,0],[2,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]],2)==True
assert victoire([[0,0,0,2,0,0,0],[1,1,2,1,0,0,0],[0,2,0,0,0,0,0],[2,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]],1)==False
