def match_nul(gril) :
    '''
    Fonction match_nul(gril):
    Renvoie True si la partie est nulle,
    c'est à dire si la ligne du haut est remplie,
    False sinon .
    
    '''
    gril=gril[::-1]
    for i in gril[-1]:
        if i==0:
            return False
    return True
assert match_nul ([[0,1,1,1],[1,1,1,1],[1,1,1,1]])==False
assert match_nul ([[1,1,1,1],[1,1,1,1],[1,1,1,1]])==True
