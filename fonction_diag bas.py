def diag_bas(gril,j,lig,col):
    '''
    Fonction diag-bas (gril,j,lig,col):
    Déterminer di il y a un alignement diagonal vers le bas
    4 pions du joueur j à partir de la case (lig, col).
    Arguments :
        gril la grille avec les pions.
        j le joueur, un entier avec la valeur 1 ou 2
        lig la ligne, un entier avec la valeur entre 0 et 2
        col est la colonne, un entier avec la valeur entre 0 et 6
    Renvoie True si c'est le cas, False sinon.
    '''
    nbr=0
    if col<=3:
        for i in range (4):
            if gril[lig+i][col+i]==j:
                nbr=nbr+1
    if nbr==4:
        return True
    nbr=0
    if col>=3:
        for i in range (4):
            if gril[lig+i][col-i]==j:
                nbr=nbr+1
            
    if nbr==4:
        return True
    return False
