def jouer (gril,j,col) :
    ''' 
    Fonction jouer (gril,j,col) :
    Fonction qui joue un coup du joueur j dans la colonne col de la grille.
    arguments:
        gril est la grille de 5 6 avec les pions des joueurs
        j est un entier qui a la valeur 1 ou 2 suivant le joueur
        col est un entier entre 0 et 6, désigne une colonne non pleine de la grille.
    Si j vaut 1 la première case vide de la colonne col prendra la valeur 1 
    Si j vaut 2 la première case vide de la colonne col prendra la valeur 2
    
    '''
    gril=gril[::-1]
    for i in gril :
        if i[col]==0:
            i[col]=j
            return gril[::-1]
    return gril
    
