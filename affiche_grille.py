def affiche(gril):
    '''
Fonction affiche(gril) : affiche une grille de 6 lignes sur 7 colonnes. 
La fonction prend en argument un tableau de taille 6 x 7.
Une ligne est nitée lig et prend une valeur entre 0 et 5.
Une ligne 0 est située en bas. 
Une colonne est située  col et prend une valeur entre 0 et 6.
la colonne 0 est située à gauche.
Dans la grill :
la valeur 0 représente une case vide, représentée par un.
la valeur 1 représente un pion du joueur 1, représenté par un x.
La valeur 2 représente un pion du joueur 2, représenté par un 0.
'''
    liste="|"
    print("  ",0,1,2,3,4,5,6)
    print("  ---------------")
    col=5
    for i in range (6):
        liste="|"
        for j in range (7):
        
            if gril[i][j]==0:
                liste=liste+ "."
            if gril[i][j]==1:
                liste= liste+"♦"
            if gril[i][j]==2:
                liste= liste+"o"
            liste=liste+"|"
        print(col,liste)
        col=col-1
    print("  ---------------")

            
affiche(grille_vide())
assert grille_vide()==[[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]]

