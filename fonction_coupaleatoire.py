def coup_aleatoire(gril,j):
    '''
    Fonction coup_aleatoire(gril,j):
    joue un coup aléatoire pour le joueur j
    on suppose la grille non pleine,
    condition indispensable pour ne pas se trouver dans une boucle infinie ! 
    '''
    if match_nul(gril)==False:
        fin=0
        from random import randint
        while fin==0:
            x=randint(0,6)
            if coup_possible(gril,x)==True:
                jouer(gril,j,x)
                fin=1
        return gril

